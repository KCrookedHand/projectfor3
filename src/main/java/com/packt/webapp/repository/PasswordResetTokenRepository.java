package com.packt.webapp.repository;

import org.springframework.data.repository.CrudRepository;

import com.packt.webapp.domain.PasswordResetToken;

public interface PasswordResetTokenRepository extends CrudRepository<PasswordResetToken, Long>{

	PasswordResetToken findByToken(String token);
}
