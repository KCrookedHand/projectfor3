package com.packt.webapp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.packt.webapp.domain.GameRequest;
import com.packt.webapp.domain.User;

public interface GameRequestRepository extends CrudRepository<GameRequest, Long> {

	List<GameRequest> findGameRequestByRequestedUser(User requestedUser);

	GameRequest findGameRequestById(long gameRequestId);
}