package com.packt.webapp.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.packt.webapp.domain.Game;
import com.packt.webapp.domain.User;

@Transactional
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

	User findUserById(long userId);

	User findUserByEmail(String email);

	User findUserByUsername(String username);

	@Override
	List<User> findAll();
	
	@Query("SELECT g FROM Game g join g.users u where u.id = :userid and g.dateTime > :now")
	List<Game> findUserGamesByIdByDateTimeGreaterThanNow(@Param("userid") long userid, @Param("now") LocalDateTime now);
	
	@Query("SELECT g FROM Game g join g.users u where u.id = :userid and g.dateTime < :now")
	List<Game> findUserGamesByIdByDateTimeLessThanNow(@Param("userid") long userid, @Param("now") LocalDateTime now);
}
