package com.packt.webapp.repository;

import org.springframework.data.repository.CrudRepository;

import com.packt.webapp.domain.Opinion;

public interface OpinionRepository extends CrudRepository<Opinion, Long> {

}
