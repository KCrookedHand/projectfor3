package com.packt.webapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.packt.webapp.domain.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
