package com.packt.webapp.repository;

import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.packt.webapp.domain.Game;
import com.packt.webapp.domain.User;

@Transactional
@Repository
public interface GameRepository extends CrudRepository<Game, Long> {

	List<Game> findAll();
	
	Page<Game> findByDateTimeGreaterThanOrderByDateTimeAsc(LocalDateTime dateTime, Pageable pageable);

	List<Game> findFirst6ByOrderByDateTimeAsc();

	Game findGameById(long gameId);

	@Query("SELECT u FROM User u join u.games g where g.id = :gameid order by u.userInfos.lastName")
	List<User> findGameUsersByIdOrderByLastName(@Param("gameid") long gameid);
}
