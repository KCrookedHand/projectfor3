package com.packt.webapp.repository;

import org.springframework.data.repository.CrudRepository;

import com.packt.webapp.domain.User;
import com.packt.webapp.domain.VerificationToken;

public interface VerificationTokenRepository extends CrudRepository<VerificationToken, Long> {

	VerificationToken findByToken(String token);

	VerificationToken findByUser(User user);
}
