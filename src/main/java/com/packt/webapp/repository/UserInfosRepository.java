package com.packt.webapp.repository;

import org.springframework.data.repository.CrudRepository;

import com.packt.webapp.domain.UserInfos;

public interface UserInfosRepository extends CrudRepository<UserInfos, Long> {

}
