package com.packt.webapp.validator;

import java.time.LocalDateTime;
import java.time.temporal.Temporal;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FutureValidator implements ConstraintValidator<Future, Temporal> {

	@Override
	public void initialize(Future constraintAnnotation) {
	}
	
	@Override
	public boolean isValid(Temporal value, ConstraintValidatorContext context){
		if(value == null){
			return true;
		}
		LocalDateTime ldt = LocalDateTime.from(value);
		if ( ldt.isAfter(LocalDateTime.now())) {
			return true;
		}
		return false;
	}

}
