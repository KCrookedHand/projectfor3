package com.packt.webapp.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.packt.webapp.domain.dto.ResetPasswordDTO;

@Component
public class PasswordResetValidator implements Validator{

	public boolean supports(Class<?> aClass) {
		return ResetPasswordDTO.class.isAssignableFrom(aClass);
	}
	
	@Override
	public void validate(Object o, Errors errors) {
		ResetPasswordDTO passwordForm = (ResetPasswordDTO) o;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmNewPassword", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newPassword", "NotEmpty");
		if(!passwordForm.getNewPassword().equals(passwordForm.getConfirmNewPassword())){
			errors.rejectValue("passwordConfirm", "Diff.userForm.passwordConfirm");
		}
	}
}
