package com.packt.webapp.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.packt.webapp.domain.dto.ChangePasswordDTO;

@Component
public class PasswordChangeValidator implements Validator{

	public boolean supports(Class<?> aClass) {
		return ChangePasswordDTO.class.isAssignableFrom(aClass);
	}
	
	@Override
	public void validate(Object o, Errors errors) {
		ChangePasswordDTO passwordForm = (ChangePasswordDTO) o;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "oldPassword", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newPassword", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword", "NotEmpty");
		if(!passwordForm.getNewPassword().equals(passwordForm.getConfirmPassword())){
			errors.rejectValue("confirmPassword", "Diff.userForm.passwordConfirm");
		}
	}
}
