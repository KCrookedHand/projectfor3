package com.packt.webapp.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@NotBlank(message = "Maybe some username?")
	private String username;
	@NotBlank(message = "You better choose some password!")
	private String password;
	@Transient
	@NotBlank(message = "Retype your password!", groups = User.class)
	private String passwordConfirm;
	@NotBlank(message = "Here goes an email.")
	@Email
	private String email;
	@NotNull(message = "Type your birthday. You won't regret!")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate birthday;
	private boolean enabled = false;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_informations")
	private UserInfos userInfos;

	@OneToMany(mappedBy = "writtenTo")
	private List<Opinion> opinions;
	@OneToMany(mappedBy = "author")
	private List<Opinion> authoredOpinions;
	@ManyToMany(mappedBy = "users")
	private Set<Game> games;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "gameOwner", orphanRemoval = true)
	private List<Game> gamesOwned;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "requestingUser")
	private Set<GameRequest> requestsMade;
	@ElementCollection
	private Set<Long> ratedGamesId = new HashSet<Long>();
	@ManyToMany
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles;

	private Integer skillValue = 0;
	private Integer socialValue = 0;
	private Integer counter = 1;

	public long getId() {
		return this.id;
	}

	public String getUsername() {
		return this.username;
	}

	public String getPassword() {
		return this.password;
	}

	public String getPasswordConfirm() {
		return this.passwordConfirm;
	}

	public String getEmail() {
		return email;
	}

	public LocalDate getBirthday() {
		return birthday;
	}
	
	public long getAge(){
		return ChronoUnit.YEARS.between(birthday, LocalDateTime.now());
	}

	public List<Opinion> getOpinions() {
		return this.opinions;
	}

	public List<Opinion> getAuthoredOpinions() {
		return this.authoredOpinions;
	}

	public Integer getCounter() {
		return this.counter;
	}

	public BigDecimal getOverallRating() {
		BigDecimal temp = BigDecimal.valueOf((double) (skillValue + socialValue) / (2 * counter));
		temp = temp.setScale(2, BigDecimal.ROUND_HALF_UP);
		return temp;
	}

	public BigDecimal getSkillRating() {
		BigDecimal temp = BigDecimal.valueOf((double) skillValue / counter);
		temp = temp.setScale(2, BigDecimal.ROUND_HALF_UP);
		return temp;
	}

	public Set<GameRequest> getRequestsMade() {
		return requestsMade;
	}

	public BigDecimal getSocialRating() {
		BigDecimal temp = BigDecimal.valueOf((double) socialValue / counter);
		temp = temp.setScale(2, BigDecimal.ROUND_HALF_UP);
		return temp;
	}

	public Set<Role> getRoles() {
		return this.roles;
	}

	public Set<Game> getGames() {
		return this.games;
	}

	public List<Game> gamesOwned() {
		return this.gamesOwned;
	}

	public Set<Long> getRatedGamesId() {
		return ratedGamesId;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public void setOpinions(List<Opinion> opinions) {
		this.opinions = opinions;
	}

	public void setAuthoredOpinions(List<Opinion> opinions) {
		this.opinions = opinions;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public void setGames(Set<Game> games) {
		 this.games = games;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	public void setValues(Integer skill, Integer social) {
		this.skillValue = this.skillValue + skill;
		this.socialValue = this.socialValue + social;
		counter++;
	}

	public void setGamesOwned(List<Game> gamesOwned) {
		this.gamesOwned = gamesOwned;
	}

	public void setRequestsMade(Set<GameRequest> requestsMade) {
		this.requestsMade = requestsMade;
	}

	public void setRatedGamesId(Set<Long> ratedGamesId) {
		this.ratedGamesId = ratedGamesId;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public UserInfos getUserInfos() {
		return userInfos;
	}

	public void setUserInfos(UserInfos userInfos) {
		this.userInfos = userInfos;
	}
}