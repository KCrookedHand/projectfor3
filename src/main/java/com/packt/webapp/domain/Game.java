package com.packt.webapp.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.packt.webapp.validator.Future;

@Entity
@Table(name = "game")
public class Game {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@NotNull(message="Give us time to prepare!")
	@DateTimeFormat(iso = ISO.DATE_TIME)
	@Future
	private LocalDateTime dateTime;
	@NotBlank(message="Where to come?")
	private String address;
	@NotBlank(message="Something about the pitch?")
	private String placeDescription;
	@NotNull(message="How expensive it is?")
	private BigDecimal price;
	@NotNull(message="How many people will come?")
	private Integer slots;
	@Transient
	private Integer playersCount = 1;

	@ManyToOne
	@JoinColumn(name = "game_owner")
	private User gameOwner;
	@ManyToMany
	@JoinTable(name = "game_user", joinColumns = @JoinColumn(name = "game_id"), inverseJoinColumns = @JoinColumn(name = "user_id"))
	private Set<User> users = new HashSet<>();

	// getters

	public long getId() {
		return this.id;
	}

	public User getGameOwner() {
		return this.gameOwner;
	}

	public LocalDateTime getDateTime() {
		return this.dateTime;
	}
	
	public long getDateTimeSinceEpoch(){
		return this.dateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
	}

	public String getAddress() {
		return this.address;
	}

	public String getPlaceDescription() {
		return this.placeDescription;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public Integer getSlots() {
		return this.slots;
	}

	public Integer getPlayersCount() {
		return this.playersCount;
	}

	public Set<User> getUsers() {
		return this.users;
	}

	// setters

	public void setId(long id) {
		this.id = id;
	}

	public void setGameOwner(User gameOwner) {
		this.gameOwner = gameOwner;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setPlaceDescription(String placeDescription) {
		this.placeDescription = placeDescription;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public void setSlots(Integer slots) {
		this.slots = slots;
	}

	public void setPlayersCount(Integer playersCount) {
		this.playersCount = playersCount;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	@Override
	public boolean equals(Object that) {
		return EqualsBuilder.reflectionEquals(this, that, "id", "date", "address");
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this, "id", "date", "address");
	}
}
