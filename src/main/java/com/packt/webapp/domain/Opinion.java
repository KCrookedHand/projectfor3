package com.packt.webapp.domain;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "opinions")
public class Opinion {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@NotBlank
	private String content;
	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDateTime date;
	@ManyToOne
	@JoinColumn(name = "commented_user")
	private User writtenTo;
	@ManyToOne
	@JoinColumn(name = "authored_user")
	private User author;
	@NotNull
	private Integer skillRate;
	@NotNull
	private Integer socialRate;

	public long getId() {
		return this.id;
	}

	public String getContent() {
		return this.content;
	}

	public LocalDateTime getDate() {
		return this.date;
	}

	public User getAuthor() {
		return this.author;
	}

	public User getWrittenTo() {
		return this.writtenTo;
	}

	public Integer getSkillRate() {
		return this.skillRate;
	}

	public Integer getSocialRate() {
		return this.socialRate;
	}

	public void setIt(long id) {
		this.id = id;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public void setWrittenTo(User writtenTo) {
		this.writtenTo = writtenTo;
	}

	public void setSkillRate(Integer skillRate) {
		this.skillRate = skillRate;
	}

	public void setSocialRate(Integer socialRate) {
		this.socialRate = socialRate;
	}
}
