package com.packt.webapp.domain;

import java.util.List;

public class OpinionForm {

	private List<Opinion> opinionList;

	public List<Opinion> getOpinionList() {
		return opinionList;
	}

	public void setOpinionList(List<Opinion> opinionList) {
		this.opinionList = opinionList;
	}
}
