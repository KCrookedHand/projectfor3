package com.packt.webapp.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "game_request")
public class GameRequest {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@OneToOne
	@JoinColumn(name = "game_id")
	private Game requestedGame;
	@OneToOne
	@JoinColumn(name = "requesting_user")
	private User requestingUser;
	@OneToOne
	@JoinColumn(name = "requested_user")
	private User requestedUser;
	private String text;

	// getters

	public long getId() {
		return this.id;
	}

	public Game getRequestedGame() {
		return this.requestedGame;
	}

	public User getRequestingUser() {
		return this.requestingUser;
	}

	public User getRequestedUser() {
		return requestedUser;
	}

	public String getText() {
		return this.text;
	}

	// setters

	public void setId(long id) {
		this.id = id;
	}

	public void setRequestedGame(Game requestedGame) {
		this.requestedGame = requestedGame;
	}

	public void setRequestingUser(User requestingUser) {
		this.requestingUser = requestingUser;
	}

	public void setRequestedUser(User requestedUser) {
		this.requestedUser = requestedUser;
	}

	public void setText(String text) {
		this.text = text;
	}
}
