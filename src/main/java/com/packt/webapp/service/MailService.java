package com.packt.webapp.service;

import org.springframework.mail.SimpleMailMessage;

import com.packt.webapp.domain.User;

public interface MailService {

	void send(SimpleMailMessage mail);

	void sendPasswordResetTokenMail(String token, User user);

	void sendConfirmRegistrationMail(String token, User user);

	void sendAgainConfirmRegistrationMail(String token, User user);

	SimpleMailMessage createPasswordResetTokenMail(String token, User user);

	SimpleMailMessage createConfirmRegistrationMail(String token, User user);

}