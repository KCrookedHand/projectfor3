package com.packt.webapp.service.impl;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.packt.webapp.domain.Game;
import com.packt.webapp.domain.GameRequest;
import com.packt.webapp.domain.User;
import com.packt.webapp.repository.GameRepository;
import com.packt.webapp.repository.GameRequestRepository;
import com.packt.webapp.repository.UserRepository;
import com.packt.webapp.service.GameService;

@Service
public class GameServiceImpl implements GameService {

	private GameRepository gameRepository;
	private UserRepository userRepository;
	private GameRequestRepository gameRequestRepository;

	@Autowired
	public GameServiceImpl(GameRepository gameRepository, UserRepository userRepository,
			GameRequestRepository gameRequestRepository) {
		this.gameRepository = gameRepository;
		this.userRepository = userRepository;
		this.gameRequestRepository = gameRequestRepository;
	}

	@Override
	public List<Game> findAll() {
		return gameRepository.findAll();
	}

	@Override
	public Page<Game> findByDateTimeGreaterThanOrderByDateTimeAsc(LocalDateTime dateTime, Pageable pageable) {
		return gameRepository.findByDateTimeGreaterThanOrderByDateTimeAsc(dateTime, pageable);
	}

	@Override
	public List<Game> findFirst6ByOrderByDateTimeAsc() {
		List<Game> list = gameRepository.findFirst6ByOrderByDateTimeAsc();
		Iterator<Game> iter = list.iterator();
		while (iter.hasNext()) {
			Game game = iter.next();
			if (game.getDateTime().isBefore(LocalDateTime.now())) {
				iter.remove();
			}
		}
		return list;
	}

	@Override
	public Game findGameById(long gameId) {
		return gameRepository.findGameById(gameId);
	}

	@Override
	public void save(Game game) {
		User loggedUser = userRepository
				.findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		game.getUsers().add(loggedUser);
		game.setGameOwner(loggedUser);
		game.setPlayersCount(1);
		game.setSlots(game.getSlots() - 1);
		gameRepository.save(game);
	}

	@Override
	public void addRequest(long gameId) {
		GameRequest req = new GameRequest();

		User requesting = userRepository
				.findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		Game requestedGame = gameRepository.findGameById(gameId);
		User requested = requestedGame.getGameOwner();

		req.setRequestingUser(requesting);
		req.setRequestedGame(requestedGame);
		req.setRequestedUser(requested);
		req.setText("niezle");
		gameRequestRepository.save(req);
	}

	@Override
	public List<GameRequest> getUserRequests() {
		User logged = userRepository
				.findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		return gameRequestRepository.findGameRequestByRequestedUser(logged);
	}

	@Override
	public void addPlayerToGame(long requestId) {
		GameRequest req = gameRequestRepository.findGameRequestById(requestId);
		Game game = req.getRequestedGame();
		User userToBeAdded = req.getRequestingUser();
		if (game.getUsers().size() < game.getSlots()) {
			game.getUsers().add(userToBeAdded);
			gameRequestRepository.save(req);
			gameRequestRepository.delete(req);
		}

	}

	public void deleteRequest(long requestId) {
		GameRequest req = gameRequestRepository.findGameRequestById(requestId);
		gameRequestRepository.delete(req);
	}

	public boolean canUserJoinGame(long gameId) {
		User logged = userRepository
				.findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		if (logged == null) {
			return false;
		}
		
		Game game = gameRepository.findGameById(gameId);
		if (isLoggedUserAGameOwner(logged, game)) {
			return false;
		} else if (isUserAlreadyInThisGame(logged, game)) {
			return false;
		}
		
		for (GameRequest gameRequest : logged.getRequestsMade()) {
			if (gameRequest.getRequestedGame().equals(game)) {
				return false;
			}
		}
		return true;
	}

	public List<User> findGameUsersByIdOrderByLastName(long gameId) {
		return gameRepository.findGameUsersByIdOrderByLastName(gameId);
	}

	
	
	private boolean isLoggedUserAGameOwner(User logged, Game game) {
		if (logged.getId() == game.getGameOwner().getId()) {
			return true;
		}
		return false;
	}
	
	private boolean isUserAlreadyInThisGame(User logged, Game game) {
		if(logged.getGames().contains(game)){
			return true;
		}
		return false;
	}
}