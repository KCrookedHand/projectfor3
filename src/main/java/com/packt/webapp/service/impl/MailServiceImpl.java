package com.packt.webapp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.packt.webapp.domain.User;
import com.packt.webapp.service.MailService;

@Service
public class MailServiceImpl implements MailService {

	@Autowired
	private JavaMailSender mailSender;

	@Override
	public void sendPasswordResetTokenMail(String token, User user) {
		this.send(this.createPasswordResetTokenMail(token, user));
	}

	@Override
	public void sendConfirmRegistrationMail(String token, User user) {
		this.send(this.createConfirmRegistrationMail(token, user));
	}

	@Override
	public void sendAgainConfirmRegistrationMail(String token, User user) {
		this.send(createConfirmRegistrationMail(token, user));
	}

	@Override
	public SimpleMailMessage createPasswordResetTokenMail(String token, User user) {
		SimpleMailMessage email = new SimpleMailMessage();

		String resetUrl = "http://localhost/changePassword?id=" + user.getId() + "&token=" + token;
		String message = "Your password reset token: ";

		email.setSubject("43ForThree Password reset request");
		email.setText(message + "\r\n" + resetUrl);
		email.setTo(user.getEmail());
		email.setFrom("me");
		return email;
	}

	@Override
	public SimpleMailMessage createConfirmRegistrationMail(String token, User user) {
		SimpleMailMessage email = new SimpleMailMessage();

		String confirmationUrl = "http://localhost:8080/registrationConfirm.html?token=" + token;
		String message = "Confirm registration visiting this link: ";

		email.setSubject("Confirm your registration on 34ForThree");
		email.setText(message + "\r\n" + confirmationUrl);
		email.setTo(user.getEmail());

		return email;
	}

	@Override
	public void send(SimpleMailMessage mail) {
		mailSender.send(mail);
	}
}
