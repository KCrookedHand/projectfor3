package com.packt.webapp.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.packt.webapp.domain.Game;
import com.packt.webapp.domain.GameRequest;
import com.packt.webapp.domain.Opinion;
import com.packt.webapp.domain.OpinionForm;
import com.packt.webapp.domain.PasswordResetToken;
import com.packt.webapp.domain.Role;
import com.packt.webapp.domain.User;
import com.packt.webapp.domain.UserInfos;
import com.packt.webapp.domain.VerificationToken;
import com.packt.webapp.domain.dto.ChangePasswordDTO;
import com.packt.webapp.repository.GameRepository;
import com.packt.webapp.repository.OpinionRepository;
import com.packt.webapp.repository.PasswordResetTokenRepository;
import com.packt.webapp.repository.RoleRepository;
import com.packt.webapp.repository.UserRepository;
import com.packt.webapp.repository.VerificationTokenRepository;
import com.packt.webapp.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private UserRepository userRepository;
	private RoleRepository roleRepository;
	private OpinionRepository opinionRepository;
	private GameRepository gameRepository;
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	private SessionFactory sessionFactory;
	private VerificationTokenRepository tokenRepository;
	private PasswordResetTokenRepository passwordResetTokenRepository;

	@Autowired
	public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository,
			OpinionRepository opinionRepository, GameRepository gameRepository,
			BCryptPasswordEncoder bCryptPasswordEncoder, SessionFactory sessionFactory,
			VerificationTokenRepository tokenRepository, PasswordResetTokenRepository passwordResetTokenRepository) {
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.opinionRepository = opinionRepository;
		this.gameRepository = gameRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.sessionFactory = sessionFactory;
		this.tokenRepository = tokenRepository;
		this.passwordResetTokenRepository = passwordResetTokenRepository;
	}

	@Override
	public void save(User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setRoles(new HashSet<Role>(roleRepository.findAll()));
		userRepository.save(user);
	}

	@Override
	public List<User> findUsers() {
		return userRepository.findAll();
	}

	@Override
	public User findUserById(long userId) {
		return userRepository.findUserById(userId);
	}

	@Override
	public User findUserByEmail(String email) {
		return userRepository.findUserByEmail(email);
	}

	@Override
	public User findUserByUsername(String username) {
		return userRepository.findUserByUsername(username);
	}

	@Override
	public void addOpinion(OpinionForm opForm, long gameId) {

		User author = userRepository
				.findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		List<User> users = gameRepository.findGameUsersByIdOrderByLastName(gameId);
		users.remove(author);
		author.getRatedGamesId().add(gameId);
		userRepository.save(author);

		User commented;

		for (int i = 0; i < users.size(); i++) {
			commented = users.get(i);
			commented.setValues(opForm.getOpinionList().get(i).getSkillRate(),
					opForm.getOpinionList().get(i).getSocialRate());
			userRepository.save(commented);
			opForm.getOpinionList().get(i).setAuthor(author);
			opForm.getOpinionList().get(i).setWrittenTo(users.get(i));
			opForm.getOpinionList().get(i).setDate(LocalDateTime.now());

			opinionRepository.save(opForm.getOpinionList().get(i));
		}

	}

	@Override
	public List<Game> getUserIncomingGames(long userId) {
		User user = findUserById(userId);
		LocalDateTime currDateTime = LocalDateTime.now();
		return userRepository.findUserGamesByIdByDateTimeGreaterThanNow(user.getId(), currDateTime);
	}

	@Override
	public List<Game> getUserPastGames(long userId) {
		User user = findUserById(userId);
		LocalDateTime currDateTime = LocalDateTime.now();
		return userRepository.findUserGamesByIdByDateTimeLessThanNow(user.getId(), currDateTime);
	}

	@Override
	public List<Game> getUserRequestedGames(long userId) {
		List<Game> requested = new ArrayList<Game>();
		for (GameRequest gamerq : findUserById(userId).getRequestsMade()) {
			requested.add(gamerq.getRequestedGame());
		}
		return requested;
	}

	public OpinionForm prepareOpinions(long gameId) {
		User logged = findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		List<User> users = new ArrayList<User>(gameRepository.findGameUsersByIdOrderByLastName(gameId));
		users.remove(logged);

		List<Opinion> opinions = new ArrayList<Opinion>();
		for (int i = 0; i < users.size(); i++) {
			Opinion op = new Opinion();
			op.setWrittenTo(users.get(i));
			opinions.add(op);
		}

		OpinionForm opForm = new OpinionForm();
		opForm.setOpinionList(opinions);

		return opForm;
	}

	public void createVerificationToken(User user, String token) {
		VerificationToken myToken = new VerificationToken(user, token);
		tokenRepository.save(myToken);
	}

	public VerificationToken getVerificationToken(String token) {
		return tokenRepository.findByToken(token);
	}

	public VerificationToken generateNewVerificationToken(String existingToken) {
		VerificationToken token = tokenRepository.findByToken(existingToken);
		token.updateToken(UUID.randomUUID().toString());
		token = tokenRepository.save(token);
		return token;
	}

	@Transactional
	public void enableUser(User user, VerificationToken token) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("update User set enabled=:enabled where id=:userId");
		query.setParameter("enabled", true);
		query.setParameter("userId", user.getId());
		query.executeUpdate();
	}

	public void createPasswordToken(User user, String token) {
		PasswordResetToken myToken = new PasswordResetToken(token, user);
		passwordResetTokenRepository.save(myToken);
	}

	public String validatePasswordResetToken(long id, String token, HttpServletRequest request) {
		PasswordResetToken passToken = passwordResetTokenRepository.findByToken(token);

		if ((passToken == null) || (passToken.getUser().getId() != id)) {
			return "invalidToken";
		}

		LocalDateTime now = LocalDateTime.now();
		if (passToken.getExpiryDate().isBefore(now)) {
			return "expired";
		}

		User user = passToken.getUser();
		Authentication auth = new UsernamePasswordAuthenticationToken(user, null,
				Arrays.asList(new SimpleGrantedAuthority("CHANGE_PASSWORD_PRIVILEGE")));
		SecurityContextHolder.getContext().setAuthentication(auth);

		HttpSession session = request.getSession(true);
		session.setAttribute("SPRING_SECURITY_CONTEXT", SecurityContextHolder.getContext());

		return null;
	}

	public void changeUserPassword(User user, String password) {
		user.setPassword(bCryptPasswordEncoder.encode(password));
		userRepository.save(user);
	}

	public String changePassword(ChangePasswordDTO passForm) {
		User user = findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		if (bCryptPasswordEncoder.matches(passForm.getOldPassword(), user.getPassword())) {
			user.setPassword(bCryptPasswordEncoder.encode(passForm.getNewPassword()));
			userRepository.save(user);
			return null;
		}
		return "Old password is different than your password";
	}

	public void saveDetails(long userId, UserInfos details) {
		User user = userRepository.findUserById(userId);
		if (user.getUserInfos() != null) {
			details.setId(user.getUserInfos().getId());
		}
		user.setUserInfos(details);
		userRepository.save(user);
	}

}
