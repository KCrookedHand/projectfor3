package com.packt.webapp.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.packt.webapp.domain.Game;
import com.packt.webapp.domain.OpinionForm;
import com.packt.webapp.domain.User;
import com.packt.webapp.domain.UserInfos;
import com.packt.webapp.domain.VerificationToken;
import com.packt.webapp.domain.dto.ChangePasswordDTO;

public interface UserService {

	void save(User user);
	
	void saveDetails(long userId, UserInfos details);

	List<User> findUsers();

	User findUserById(long userId);

	User findUserByEmail(String email);

	User findUserByUsername(String username);

	void addOpinion(OpinionForm opForm, long gameId);

	List<Game> getUserIncomingGames(long userId);

	List<Game> getUserPastGames(long userId);

	List<Game> getUserRequestedGames(long userId);
	
	OpinionForm prepareOpinions(long gameId);

	void createVerificationToken(User user, String token);

	VerificationToken getVerificationToken(String token);

	void enableUser(User user, VerificationToken token);

	VerificationToken generateNewVerificationToken(String existingToken);

	void createPasswordToken(User user, String token);

	String validatePasswordResetToken(long id, String token, HttpServletRequest request);

	void changeUserPassword(User user, String password);

	String changePassword(ChangePasswordDTO passForm);
}
