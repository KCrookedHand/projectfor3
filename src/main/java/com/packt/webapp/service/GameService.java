package com.packt.webapp.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.packt.webapp.domain.Game;
import com.packt.webapp.domain.GameRequest;
import com.packt.webapp.domain.User;

public interface GameService {

	List<Game> findAll();
	
	Page<Game> findByDateTimeGreaterThanOrderByDateTimeAsc(LocalDateTime dateTime, Pageable pageable);

	List<Game> findFirst6ByOrderByDateTimeAsc();

	Game findGameById(long gameId);

	void save(Game game);

	void addRequest(long gameId);

	List<GameRequest> getUserRequests();

	void addPlayerToGame(long requestId);
	
	void deleteRequest(long requestId);

	boolean canUserJoinGame(long gameId);

	List<User> findGameUsersByIdOrderByLastName(long gameId);
}
