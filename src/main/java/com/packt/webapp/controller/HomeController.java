package com.packt.webapp.controller;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import com.packt.webapp.domain.User;
import com.packt.webapp.domain.VerificationToken;
import com.packt.webapp.domain.dto.ResetPasswordDTO;
import com.packt.webapp.service.GameService;
import com.packt.webapp.service.MailService;
import com.packt.webapp.service.UserService;
import com.packt.webapp.validator.PasswordResetValidator;
import com.packt.webapp.validator.UserValidator;

@Controller
@RequestMapping("/")
public class HomeController {

	private UserService userService;
	private GameService gameService;
	private UserValidator userValidator;
	private PasswordResetValidator passwordResetValidator;
	private MailService mailService;

	@Autowired
	public HomeController(GameService gameService, UserService userService, UserValidator userValidator,
			MailService mailService, PasswordResetValidator passwordResetValidator) {
		this.userService = userService;
		this.gameService = gameService;
		this.userValidator = userValidator;
		this.mailService = mailService;
		this.passwordResetValidator = passwordResetValidator;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String home(Model model) {
		model.addAttribute("lastGames", gameService.findFirst6ByOrderByDateTimeAsc());
		return "main/home";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			return "redirect:/";
		}
		return "main/login";
	}

	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public String showRegistration(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			return "redirect:/";
		}
		model.addAttribute("userForm", new User());
		return "main/registration";
	}

	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public String registration(@Valid @ModelAttribute("userForm") User userForm, BindingResult bindingResult,
			Model model, WebRequest request) {
		userValidator.validate(userForm, bindingResult);

		if (bindingResult.hasErrors()) {
			return "main/registration";
		}
		userService.save(userForm);

		String token = UUID.randomUUID().toString();
		userService.createVerificationToken(userForm, token);
		mailService.sendConfirmRegistrationMail(token, userForm);

		return "redirect:/users/" + userForm.getId() + "/settings";
	}

	@RequestMapping(value = "/registrationConfirm", method = RequestMethod.GET)
	public String confirmRegistration(WebRequest request, Model model, @RequestParam("token") String token) {

		VerificationToken verificationToken = userService.getVerificationToken(token);
		if (verificationToken == null) {
			String message = new String("Invalid token");
			model.addAttribute("message", message);
			return "redirect:/badUser";
		}

		User user = verificationToken.getUser();
		LocalDateTime date = LocalDateTime.now();
		if ((verificationToken.getExpiryDate().isBefore(date))) {
			String messageValue = new String("Token expired");
			model.addAttribute("message", messageValue);
			return "redirect:/badUser";
		}
		user.setEnabled(true);
		userService.enableUser(user, verificationToken);
		return "redirect:/";
	}

	@RequestMapping(value = "/resendRegistrationToken", method = RequestMethod.GET)
	public String errorNonConfirmedUser(@RequestParam("token") String existingToken, WebRequest request) {

		VerificationToken newToken = userService.generateNewVerificationToken(existingToken);

		User user = newToken.getUser();
		mailService.sendAgainConfirmRegistrationMail(newToken.getToken(), user);

		return "redirect:/";
	}

	@RequestMapping(value = "/resetPassword")
	public String resetPassword(@RequestParam(value = "email", required = false) String email) {
		if (email == null) {
			return "account/resetPassword";
		}
		User user = userService.findUserByEmail(email);
		if (user == null) {
			// throw new UserNotFoundException();
		}

		String token = UUID.randomUUID().toString();
		userService.createPasswordToken(user, token);
		mailService.sendPasswordResetTokenMail(token, user);
		return "redirect:/";
	}

	@RequestMapping(value = "/changePassword", method = RequestMethod.GET)
	public String changePassword(@RequestParam("id") long userId, @RequestParam("token") String token, Model model,
			HttpServletRequest request) {
		String result = userService.validatePasswordResetToken(userId, token, request);

		if (result != null) {
			model.addAttribute("message", "Error jaki");
			return "redirect:/login";
		}

		return "redirect:/updatePassword";
	}

	@RequestMapping(value = "/updatePassword", method = RequestMethod.GET)
	public String showResetPasswordForm(Model model) {
		model.addAttribute("passwordForm", new ResetPasswordDTO());
		return "account/updatePassword";
	}

	@RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
	public String resetPassword(@ModelAttribute("passwordForm") ResetPasswordDTO passwordForm,
			BindingResult bindingResult, HttpServletRequest request) throws ServletException {

		HttpSession session = request.getSession();
		SecurityContextHolder.setContext((SecurityContext) session.getAttribute("SPRING_SECURITY_CONTEXT"));

		passwordResetValidator.validate(passwordForm, bindingResult);
		if (bindingResult.hasErrors()) {
			return "account/updatePassword";
		}
		User user = userService.findUserByEmail(
				((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getEmail());

		userService.changeUserPassword(user, passwordForm.getNewPassword());
		request.logout();
		return "redirect:/";
	}

	@RequestMapping(value = "/me", method = RequestMethod.GET)
	public String showMyPage(Model model) {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		if (username != null) {
			long userId = userService.findUserByUsername(username).getId();
			return "redirect:/users/" + userId;
		} else {
			return "redirect:/";
		}
	}
}
