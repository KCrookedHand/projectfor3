package com.packt.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.packt.webapp.domain.User;
import com.packt.webapp.domain.UserInfos;
import com.packt.webapp.domain.dto.ChangePasswordDTO;
import com.packt.webapp.service.GameService;
import com.packt.webapp.service.UserService;
import com.packt.webapp.validator.PasswordChangeValidator;

@Controller
@RequestMapping("/users")
@SessionAttributes("userForm")
public class UserController {

	private UserService userService;
	private GameService gameService;
	private PasswordChangeValidator passwordChangeValidator;

	@Autowired
	public UserController(UserService userService, GameService gameService,
			PasswordChangeValidator passwordChangeValidator) {
		this.userService = userService;
		this.passwordChangeValidator = passwordChangeValidator;
		this.gameService = gameService;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String showUsers(Model model) {
		model.addAttribute("users", userService.findUsers());
		return "user/users";
	}

	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	public String showUser(@PathVariable("userId") long userId, Model model) {
		model.addAttribute("user", userService.findUserById(userId));
		return "user/user";
	}

	@RequestMapping(value = "/{userId}/settings", method = RequestMethod.GET)
	public String showSettings(@PathVariable("userId") long userId, Model model) {
		User user = userService.findUserById(userId);
		if (user.getUserInfos() == null) {
			model.addAttribute("details", new UserInfos());
		} else {
			model.addAttribute("details", user.getUserInfos());
		}
		model.addAttribute("userId", userId);
		return "user/settings";
	}

	@RequestMapping(value = "/{userId}/settings", method = RequestMethod.POST)
	public String confirmSettings(@Valid @ModelAttribute("details") UserInfos details, BindingResult bindingResult,
			@PathVariable("userId") long userId, Model model) {
		if (bindingResult.hasErrors()) {
			return "user/settings";
		}
		userService.saveDetails(userId, details);
		return "redirect:/users/" + userId;
	}

	@RequestMapping(value = "/{userId}/futureGames", method = RequestMethod.GET)
	public String showUserFutureGames(@PathVariable("userId") long userId, Model model) {
		model.addAttribute("user", userService.findUserById(userId));
		model.addAttribute("games", userService.getUserIncomingGames(userId));
		return "user/games";
	}

	@RequestMapping(value = "/{userId}/pastGames", method = RequestMethod.GET)
	public String showUserPastGames(@PathVariable("userId") long userId, Model model, HttpServletRequest request) {
		model.addAttribute("user", userService.findUserById(userId));
		model.addAttribute("games", userService.getUserPastGames(userId));
		model.addAttribute("uri", request.getRequestURI());
		return "user/games";
	}

	@RequestMapping(value = "/{userId}/requestedGames", method = RequestMethod.GET)
	public String showUserRequestedGames(@PathVariable("userId") long userId, Model model) {
		model.addAttribute("user", userService.findUserById(userId));
		model.addAttribute("games", userService.getUserRequestedGames(userId));
		return "user/games";
	}

	@RequestMapping(value = "/{userId}/requests", method = RequestMethod.GET)
	public String showUserRequests(@PathVariable("userId") long userId, Model model) {
		model.addAttribute("user", userService.findUserById(userId));
		model.addAttribute("requests", gameService.getUserRequests());
		return "user/gamesRequests";
	}

	@RequestMapping(value = "/request/{requestId}/accept", method = RequestMethod.POST)
	public String acceptJoinGameRequest(@PathVariable("requestId") long requestId, Model model) {
		User logged = userService.findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		gameService.addPlayerToGame(requestId);
		return "redirect:/users/" + logged.getId() + "/requests";
	}

	@RequestMapping(value = "/request/{requestId}/reject", method = RequestMethod.POST)
	public String rejectJoinGameRequest(@PathVariable("requestId") long requestId, Model model) {
		User logged = userService.findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		gameService.deleteRequest(requestId);
		return "redirect:/users/" + logged.getId() + "/requests";
	}

	@RequestMapping(value = "/{userId}/opinions", method = RequestMethod.GET)
	public String showUserOpinions(@PathVariable("userId") long userId, Model model) {
		model.addAttribute("user", userService.findUserById(userId));
		return "user/opinions";
	}

	@RequestMapping(value = "/changePassword", method = RequestMethod.GET)
	public String showChangePassword(Model model) {
		model.addAttribute("passForm", new ChangePasswordDTO());
		return "account/changePassword";
	}

	@RequestMapping(value = "/changePassword", method = RequestMethod.POST)
	public String changePassword(@ModelAttribute("passForm") ChangePasswordDTO passForm, BindingResult bindingResult,
			Model model) {
		passwordChangeValidator.validate(passForm, bindingResult);
		if (bindingResult.hasErrors()) {
			return "account/changePassword";
		} else {
			long userId = userService
					.findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
			String message;
			if ((message = userService.changePassword(passForm)) != null) {
				model.addAttribute("message", message);
				return "account/changePassword";
			}
			return "redirect:/users/" + userId;
		}
	}

}
