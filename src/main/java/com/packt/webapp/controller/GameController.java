package com.packt.webapp.controller;

import java.time.LocalDateTime;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.packt.webapp.domain.Game;
import com.packt.webapp.domain.OpinionForm;
import com.packt.webapp.domain.User;
import com.packt.webapp.service.GameService;
import com.packt.webapp.service.UserService;

@Controller
@RequestMapping("/games")
@SessionAttributes("editGameForm")
public class GameController {

	private GameService gameService;
	private UserService userService;

	@Autowired
	public GameController(GameService gameService, UserService userService) {
		this.gameService = gameService;
		this.userService = userService;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String showAllGames(Model model, Pageable pageable) {
		Page<Game> gamePage = gameService.findByDateTimeGreaterThanOrderByDateTimeAsc(LocalDateTime.now(), pageable);
		PageWrapper<Game> page = new PageWrapper<Game>(gamePage, "/games");
		model.addAttribute("page", page);
		return "game/games";
	}

	@RequestMapping(value = "/newGame", method = RequestMethod.GET)
	public String showNewGameForm(Model model) {
		model.addAttribute("gameForm", new Game());
		return "game/newGame";
	}

	@RequestMapping(value = "/newGame", method = RequestMethod.POST)
	public String addNewGame(@Valid @ModelAttribute("gameForm") Game gameForm, BindingResult bindingResult,
			Model model) {
		if (bindingResult.hasErrors()) {
			return "game/newGame";
		}
		gameService.save(gameForm);
		return "redirect:/games";
	}

	@RequestMapping(value = "/{gameId}", method = RequestMethod.GET)
	public String showGame(@PathVariable("gameId") long gameId, Model model) {
		model.addAttribute("canJoin", gameService.canUserJoinGame(gameId));
		model.addAttribute("game", gameService.findGameById(gameId));
		return "game/game";
	}

	@RequestMapping(value = "/{gameId}/join", method = RequestMethod.POST)
	public String requestJoinGame(@PathVariable("gameId") long gameId, Model model) {
		model.addAttribute("game", gameService.findGameById(gameId));
		gameService.addRequest(gameId);
		return "redirect:/games/" + gameId;
	}

	@RequestMapping(value = "/{gameId}/players", method = RequestMethod.GET)
	public String showGamePlayers(@PathVariable("gameId") long gameId, Model model) {
		model.addAttribute("game", gameService.findGameById(gameId));
		return "game/players";
	}

	@RequestMapping(value = "/{gameId}/rate", method = RequestMethod.GET)
	public String showRatePlayersForm(@PathVariable("gameId") long gameId, Model model) {
		User logged = userService.findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		if (logged.getRatedGamesId().contains(gameId)) {
			return "redirect:/users/" + logged.getId() + "/pastGames";
		}

		model.addAttribute("gameId", gameId);
		model.addAttribute("opForm", userService.prepareOpinions(gameId));

		return "game/ratePlayers";
	}

	@RequestMapping(value = "/{gameId}/rate", method = RequestMethod.POST)
	public String addPlayersRate(@Valid @ModelAttribute("opForm") OpinionForm opForm, BindingResult bindingResult,
			@PathVariable("gameId") long gameId, Model model) {
		if (bindingResult.hasErrors()) {
			return "/games/" + gameId + "/rate";
		}
		userService.addOpinion(opForm, gameId);
		return "redirect:/me";
	}

	@RequestMapping(value = "/{gameId}/edit", method = RequestMethod.GET)
	public String showEditGameForm(@PathVariable("gameId") long gameId, Model model) {
		model.addAttribute("editGameForm", gameService.findGameById(gameId));
		return "game/editGame";
	}

	@RequestMapping(value = "/{gameId}/edit", method = RequestMethod.POST)
	public String editGame(@Valid @ModelAttribute("editGameForm") Game game, BindingResult bindingResult,
			@PathVariable("gameId") long gameId, SessionStatus status, Model model) {
		if (bindingResult.hasErrors()) {
			return "editGame";
		} else {
			gameService.save(game);
			status.setComplete();
			return "redirect:/games/" + gameId;
		}
	}
}